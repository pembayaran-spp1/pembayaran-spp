import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoPembayaranPageRoutingModule } from './info-pembayaran-routing.module';

import { InfoPembayaranPage } from './info-pembayaran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfoPembayaranPageRoutingModule
  ],
  declarations: [InfoPembayaranPage]
})
export class InfoPembayaranPageModule {}
