import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfoPembayaranPage } from './info-pembayaran.page';

describe('InfoPembayaranPage', () => {
  let component: InfoPembayaranPage;
  let fixture: ComponentFixture<InfoPembayaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPembayaranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfoPembayaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
