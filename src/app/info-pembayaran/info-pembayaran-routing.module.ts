import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoPembayaranPage } from './info-pembayaran.page';

const routes: Routes = [
  {
    path: '',
    component: InfoPembayaranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoPembayaranPageRoutingModule {}
