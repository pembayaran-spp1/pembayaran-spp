import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RiwayatPembayaranPageRoutingModule } from './riwayat-pembayaran-routing.module';

import { RiwayatPembayaranPage } from './riwayat-pembayaran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RiwayatPembayaranPageRoutingModule
  ],
  declarations: [RiwayatPembayaranPage]
})
export class RiwayatPembayaranPageModule {}
