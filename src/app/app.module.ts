import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import firebase from '@firebase/app';
var firebaseConfig = {
  apiKey: "AIzaSyAZUBDqJOKxuYWjtu6PQrlIBwLmjvt0pEk",
  authDomain: "tuitionpay.firebaseapp.com",
  projectId: "tuitionpay",
  storageBucket: "tuitionpay.appspot.com",
  messagingSenderId: "408402218102",
  appId: "1:408402218102:web:233b6abb1d93b70a7d6357",
  measurementId: "G-177B2BM6Q9"
};
firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}