import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DataSiswaPage } from './data-siswa.page';

describe('DataSiswaPage', () => {
  let component: DataSiswaPage;
  let fixture: ComponentFixture<DataSiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataSiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DataSiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
