import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import firebase from '@firebase/app';
import '@firebase/auth';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  constructor(
    private router: Router,
    public alert: AlertController,
    public modalController : ModalController,
    ) { }

  ngOnInit() {
  }

  email: string = "";
  async forgot() {
    firebase.auth().sendPasswordResetEmail(this.email)
    .then(() => {
      this.showAlert("Success","Password Reset Email Sent Successfully!");
      console.log('Password Reset Email Sent Successfully!');
      this.router.navigate(["/login"])
    })
    .catch((err)=>{
      this.showAlert("Failed","Password Reset Email Wasn't Sent Successfully!");
      console.log("Gagal")
    });
  }
  async showAlert(header:string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }
}